//1 Вкладки в секции Our services должны переключаться при нажатии мышью. Текст и картинки 
// для других вкладок вставить любые.

let tabs = document.querySelectorAll('.filter-services button');

tabs.forEach(function(tab){
    tab.addEventListener('click', function(){
        let currentTabData = document.querySelector('.our-services__desc[data-tab-content="' + this.dataset.tabTrigger + '"]');

        document.querySelector('.wrapper div.is-open').classList.remove('is-open');
        document.querySelector('.filter-services button.active').classList.remove('active');
        currentTabData.classList.add('is-open');
        this.classList.add('active');
    });
});

//2 Кнопка Load more в секции Our amazing work имитирует подгрузку с сервера новых картинок. 
// При ее нажатии в секции снизу должно добавиться еще 12 картинок (изображения можно взять любые). 
// После этого кнопка исчезает.

let galeryBtn = document.querySelector(`.btn--img`);

galeryBtn.addEventListener('click', function(){
    let hiddenGalery = document.querySelector('.work-galery_hidden');

    hiddenGalery.classList.remove('work-galery_hidden');
    hiddenGalery.classList.add('work-galery');
    this.classList.add('work-galery_hidden');
});


//3 Кнопки на вкладке Our amazing work являются "фильтрами продукции". Предварительно каждой 
// из картинок нужно присвоить одну из четырех категорий, на ваше усмотрение 
// (на макете это Graphic design, Web design, Landing pages, Wordpress). 
// При нажатии на кнопку категории необходимо показать только те картинки, 
// которые относятся к данной категории. All показывает картинки из всех категорий. 
// Категории можно переименовать, картинки для категорий взять любые.


let filterButtons = document.querySelectorAll('.work-section button');

filterButtons.forEach(function(filterButton){
    filterButton.addEventListener('click', function(){
        let filter = this.dataset.filterBtn;
        if('' === filter) {
            let items = document.querySelectorAll('.work-galery__item');
            items.forEach(function(item){
                item.classList.remove('work-galery_hidden');
            });
        } else {
            let items = document.querySelectorAll('.work-galery__item');
            items.forEach(function(item){
                item.classList.add('work-galery_hidden');
            });
            items = document.querySelectorAll('.work-galery__item[data-filter="' + filter + '"]');
            items.forEach(function(item){
                item.classList.remove('work-galery_hidden');
            });
        }
    });
});

//4 Карусель на вкладке What people say about theHam должна быть рабочей, 
// по клику как на иконку фотографии внизу, так и на стрелки вправо-влево. 
// В карусели должна меняться как картинка, так и текст. 
// Карусель обязательно должна быть с анимацией.

function peopleSaySwitch(number) {
    let scrollFaces = document.querySelectorAll('.people_say_scroll_face');
    let scrollTexts = document.querySelectorAll('.people_say_text');

    scrollFaces.forEach(function(face){
        face.classList.remove('active');
        if(number == face.dataset.scrollFaceIndex) {
            face.classList.add('active');
        }
    });

    scrollTexts.forEach(function(text){
        text.classList.remove('active');
        if(number == text.dataset.scrollTextIndex) {
            text.classList.add('active');
        }
    });
}

let scrollButtons = document.querySelectorAll('.people_say_scroll_face');

scrollButtons.forEach(function(scrollButton){
    scrollButton.addEventListener('click', function(){
        peopleSaySwitch(this.dataset.scrollFaceIndex);
    });
});

let scrollArrows = document.querySelectorAll('.people_say_scroll .btn--scroll');

scrollArrows.forEach(function(scrollArrow){
    scrollArrow.addEventListener('click', function(){
        let position = document.querySelector('.people_say_scroll_face.active').dataset.scrollFaceIndex;
        if('left' == this.dataset.scrollDirection && 1 < position) {
            position--;
        }
        if('right' == this.dataset.scrollDirection && 4 > position) {
            position++;
        }
        peopleSaySwitch(position);
    });
});
