//Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM) 

//преобразование документа в древоподобную структуру страницыб которая включает в себя родительские и дочерние елементы, в которой их можно добалять, убирать, редактлорвать.


let addList = ["Kharkiv", ["Barabachova"], "Kiev", ["Stolichnui", "Troeschina"], "Odessa",["7-kilometr"], "Lviv",["Ploscha runok"], "Dnieper",["Ozerka"]];

function toHtmlList(newList){
    if (typeof newList === 'object') {
        return `<ul>${newList.map(toHtmlList).join('')}</ul>`;
    } else {
        return `<li>${newList}</li>`;
    }
}

function createList(list, elName= document.body){ 
    let elList = "<ol>";
    elList += list.map(toHtmlList).join('');
    elList += "</ol>";
    elName.innerHTML += elList;
}

createList(addList, document.getElementsByTagName('div')[0]);


