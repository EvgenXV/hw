const person = {
  firstName: 'Evgen',
  lastName: 'Ivanytslkyy',
  makeCoffee: function () {
    console.log('macing coffe');
  },
};

// person.makeCoffee = function () {
//   console.log('macing coffe');
// };

// const student = {

//   attendLesson: function () {
//     console.log(`${this.firstName} attending lesson`);
//   },
// };
// student.__proto__ = person;

// console.log(person);
// console.log(person.firstName);

const student = Object.create(person);
console.log(person);