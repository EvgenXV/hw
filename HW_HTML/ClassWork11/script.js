function createUser(firstName, lastName, sex, age) {
    return {
      name: {
        first: firstName,
        last: lastName,
      },
      sex,
      age,
      getFullName: function () {
        return this.name.first + " " + this.name.last;
      },
      isAdult: function () {
        if (this.age > 18) {
          return true;
        } else {
          return false;
        }
      },
    };
  }
  const sayFn = function () {
    console.log("Hello");
  };
  
  function User(firstName, lastName, sex, age) {
    // this = {}
    // console.log("new.target", new.target);
    // if (!new.target) return new User(firstName, lastName, sex, age);
  
    this.firstName = firstName;
    this.lastName = lastName;
    this.sex = sex;
    this.age = age;
  }
  
  const user1 = new User("firstName1", "lastName1", "m", 30);
  // user1.newFn = () => null;
  const user2 = User("firstName2", "lastName2", "f", 17);
  
  User.prototype.getFullName = function (arg, arg2, arg3) {
    console.log("getFullName: ", this.firstName + " " + this.lastName);
    console.log("arg:", arg, arg2, arg3);
  };
  
  // User.prototype.isAdult = function () {
  //   if (this.age >= 18) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // };
  
  const bindedFn = user2.getFullName.bind(user1, 1, "string");
  console.log(bindedFn);
  bindedFn("last arg");
  bindedFn("banaanaaa!!!");
  
  const example = {
    fn() {
      console.log("->", this);
    },
  };
  
  const bindedFn2 = example.fn.bind(example);
  
  setTimeout(function () {
    bindedFn2();
  }, 4000);
  
  example.fn = null;
  
  // console.log("User1: ", User1);
  // console.log("User2: ", User2);
  
  // const user1 = new User("Chester", "Miller", "male", 34);
  
  // const user2 = new User("Avery", "Hale", "female", 14);
  
  // let obj = {
  //   fn() {
  //     console.log(this);
  //   },
  //   arrow: () => console.log(this),
  // };
  
  // call(this, arg1, arg2, arg3);
  // apply(this, [arg1, arg2, arg3, ..., argN]);
  
  // newFn = bind(this, arg1, arg2, arg3)
  