/**
 * Наявний функціонал для друку оцінок одного студента
 * Але зі збільшенням кількості студентів постало питання про
 * розширення його
 * Для цього необхідно створити функцію-конструктор Student,
 * яка буде створювати об'єкт студента та мати ті ж самі методи
 *
 * - створити за допомогою функції-конструктора ще 2х студентів
 * - вивести оцінки кожного за допомогою метода printGrades
 * - вивести середній бал кожного студента
 * - додати метод, який буде виводити оцінку по заданій технологій
 * наприклад getGrade('html') повинен виводити оцінку студента по html
 * - вивести в консоль оцінку по js першого студента та по python - третього
 *
 *
 * ADVANCED:
 * - створити окремо функцію getStudentWithHighestResults, яка буде виводити
 * ім'я та прізвище студента за найвищим середнім балом
 */
 const student = {
    firstName: "Margie",
    lastName: "Sullivan",
    sex: "female",
    grades: {
      html: 90,
      css: 60,
      js: 50,
      python: 45,
    },
    printGrades() {
      for (let key in this.grades) {
        if (this.sex === "male") {
          console.log(`По ${key} ${this.firstName} отримав ${this.grades[key]}`);
        } else {
          console.log(`По ${key} ${this.firstName} отримала ${this.grades[key]}`);
        }
      }
    },
    average: function () {
      let sum = 0;
      let subjectsCount = 0;
      for (const subject in this.grades) {
        sum += this.grades[subject];
        subjectsCount++;
      }
      return sum / subjectsCount;
    },
  };
  
//   student.printRating();
//   student.average();
  
  function Student (firstName, lastName, sex, grades){
      this.firstName = firstName;
      this.lastName = lastName;
      this.sex = sex;
    //   this.grades = grades;
    if (typeof grades !== 'object'){
        this.grades = {html: 0,
          css: 0,
          js: 0,
          python: 0}
    } else {
      this.grades = {
          html: +grades.html || 0,
          css: +grades.css || 0,
          js: +grades.js || 0,
          python: +grades.python || 0
      }
    }
  }

  const student1 = new Student ('John', 'Jonovich', 'male', { 
      html: 80, 
      css: 60, 
      js: 60, 
      python: 80,});

  const student2 = new Student ('Evgen', 'Ivanytskyy', 'mele', { 
    html: 70, 
    css: 70, 
    js: 51, 
    python: 90,
    test: true }); 

    const student3 = new Student ('Michael', 'Anderson', 'mele', { 
        html: 81, 
        css: 49, 
        js: 55, 
        python: 85, 
    });

    const student4 = new Student ('Ariya', 'Stark', 'femele', { 
        html: 65, 
        css: 45, 
        js: 52, 
        python: 79,
    });

  console.log(student1);
  console.log(student2);
  console.log(student3);
  console.log(student4);

  Student.prototype.printGrades = function () {
    for (let key in this.grades) {
      if (this.sex === "male") {
        console.log(`По ${key} ${this.firstName} отримав ${this.grades[key]}`);
      } else {
        console.log(`По ${key} ${this.firstName} отримала ${this.grades[key]}`);
      }
    }
  };

  student1.printGrades();
  student2.printGrades();
  
  Student.prototype.average = function (){
    let sum = 0;
    let subjectsCount = 0;
    for (const subject in this.grades) {
      sum += this.grades[subject];
      subjectsCount++;
    }
    return sum / subjectsCount;
  }
  console.log(student1.average());
  console.log(student2.average());
  console.log(student3.average());
  console.log(student4.average());

  Student.prototype.getGrade = function (subject) {
    if (typeof subject === "string") {
    //   if (subject in this.grades) {
        if(this.grades.hasOwnProperty(subject)) {
        return this.grades[subject];
      } else {
        console.error("Undefined name of subject");
      }
    } else {
      console.error("Error type of subject");
    }
  };
  
  console.log(student1.getGrade("html"));

  let studentArr = [student1, student2, student3, student4];

  function getStudentWithHighestResults(students) {
    if (!Array.isArray(students)) return console.error("Students is not array");
  
    if (students.length === 0) return console.error("Your array is empty");
  
    students.sort(function (studenta, studentb) {
      if (studenta.average() < studentb.average()) {
        return -1;
      }
      if (studenta.average() > studentb.average()) {
        return 1;
      }
      return 0;
    });
    return students[0];
  }

  function SimpleUser (name) {
      this.name = name;
  };
  const user = new SimpleUser('Henadiy');
  console.log(user instanceof Object);
  
//   console.log(getStudentWithHighestResults());
  console.log(student1 instanceof Student);
  