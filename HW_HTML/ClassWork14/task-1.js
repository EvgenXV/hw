class Device {
  constructor(brand, model, price) {
    this.brand = brand;
    this.model = model;
    this.price = price;
  }

  getName() {
    return `${this.brand} ${this.model} `;
  }
  getPrice() {
    return this.price * 1.2;
  }
}

class Phone extends Device {
  constructor(brand, model, price, nfc) {
    super(brand, model, price);
    this.nfc = nfc;
  }
  getDeliveryPrice() {
    const price = this.getPrice();
    return price >= 10000 ? price * 0.01 : price * 0.03;
  }
}

class Tablet extends Device {
  constructor(brand, model, price, sim) {
    super(brand, model, price);
    this.sim = sim;
  }
  getDeliveryPrice() {
    const price = this.getPrice();
    return price >= 20000 ? 0 : price * 0.01;
  }
}

class Notebook extends Device {
  getDeliveryPrice() {
    return this.getPrice() >= 30000 ? 0 : 200;
  }
}

const notebook1 = new Notebook("Huawei", "Matebook D15", 24999);
const notebook2 = new Notebook("Apple", 'Macbook Air 13"', 30000);
const notebook3 = new Notebook("Acer", "Nitro 5", 27999);

const phone1 = new Phone("Apple", "Iphone 12", 27999, true);
const phone2 = new Phone("Samsung", "Galaxy S21", 28999, true);

const tablet = new Tablet("Lenovo", "Yoga Tab", 7999, false);

console.log(notebook1.getName(), phone1.getName(), tablet.getName());
