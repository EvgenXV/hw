/**
 * Написати функціонал для тултипів
 *
 * Налаштування:
 * - текст
 * - позиціонування (зверху, справа, знизу, зліва), яке за замовчуванням зверху
 * - можливості відображення (при наведенні чи при кліку), за замовчуванням при наведенні
 *
 */

/**
 * <span class="tooltip-content">Some text</span>
 */

/**
 * placement - 'left', 'right', 'top' or 'bottom'
 * trigger - 'click', 'hover'
 */
 const POSITION_CLASS_NAME = new Map([
  ["left", "tooltip-left"],
  ["right", "tooltip-right"],
  ["down", "tooltip-down"],
  ["up", "tooltip-up"],
]);

class Tooltip {
  disable = false;
  active = false;

  constructor(selector, text, position = "top", event = "hover") {
    this.selector = selector;
    this.text = text;
    this.position = position;
    this.event = event;
  }

  render() {
    if (this.disable) return;
    let root = this.getRootElement();
    if (!!root) {
      root.classList.add("tooltip");
      root.classList.add(this.getPositionClassName());
      root.insertAdjacentHTML(
        "afterbegin",
        `<span class="tooltip-content">${this.text}</span>`
      );
    }
  }

  lock() {
    this.disable = true;
  }
  unlock() {
    this.disable = false;
  }
  show() {
    if (this.disable || this.active) return;
    let root = this.getRootElement();
    if (!!root) {
      root.classList.add("active");
      this.active = true;
    }
  }
  hide() {
    if (!this.active || this.disable) return;
    let root = this.getRootElement();
    if (!!root) {
      root.classList.remove("active");
      this.active = false;
    }
  }
  getRootElement() {
    let root = document.querySelector(this.selector);
    if (!root) console.error("Root element not found!");
    return root;
  }
  attachEvent() {
    let root = this.getRootElement();
    if ((this.event = "click")) {
      root.addEventListener("click", this.toggleVisibility);
    }
  }
  toggleVisibility() {
    if (this.disable) return;

    let root = this.getRootElement();
    if (!!root) {
      root.classList.toggle("active");
      this.active = !this.active;
    }
  }
  getPositionClassName() {
    return POSITION_CLASS_NAME.has(this.position)
      ? POSITION_CLASS_NAME.get(this.position)
      : POSITION_CLASS_NAME.get("top");
  }
}
const tooltip = new Tooltip("#btn-1", "Test text", 123, "click");
tooltip.render();
tooltip.attachEvent();
